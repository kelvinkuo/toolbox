#!/bin/bash

# 删除/tmp/下和所有子目录中的*.log
find /tmp -type f -path '*.log' -exec rm {} \;
# 删除/tmp/下和所有子目录中的*.log且文件更改时间是24小时之前的
find /tmp -type f -path '*.log' -mtime +0 -exec rm {} \;
# 删除/tmp/下和所有子目录中的*.log且文件更改时间是24小时之内
find /tmp -type f -path '*.log' -mtime 0 -exec rm {} \;
