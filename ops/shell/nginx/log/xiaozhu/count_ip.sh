#! /bin/bash
if [ -z "$1" ]; then
    echo "please input a nginx access log path"
    exit
fi

awk '{print $1}'  $1 | sort | uniq -c | sort -rn
