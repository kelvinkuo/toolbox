#!/bin/bash

if (( $EUID != 0 )); then
    echo "please use root account or run with sudo"
    exit
fi

users=$(cut -d: -f1 /etc/passwd)
i=0

for user in $users
do
    cronlist="$(crontab -u "${user}" -l 2>/dev/null)"
    
    if [[ $? -eq 0 ]]; then
	echo "---------------------- ${user} ----------------------"
	echo "${cronlist[@]}"
	echo " "
    fi
done
