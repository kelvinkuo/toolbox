#! /bin/bash
if [ -z "$1" ]; then
    echo "please input a workspace name"
    exit
fi
echo "init workspace $1"
#init dir
mkdir $1 && cd $1
mkdir src
mkdir pkg
mkdir bin

#promot set env
PWD=$(pwd)
echo "please add below to your shell profile:"
echo "export GOPATH=\$GOPATH:$PWD"
