# coding=utf-8
# Creator        :kelvin
# Date           :2016.01.22
# Description    :check domain is available
#

import time
import pythonwhois
import datetime
# import english_words
import logging


def word_domain_prefix(pref):
    """
    domain = pref + english word
    :param pref: keyword
    :return: list of all available domains
    """
    for word in english_words.words:
        logging.debug('check %s' % word)
        time.sleep(0.5)
        if is_domain_available(pref+word):
            yield pref+word


def word_domain_suffix(suf, begin=None):
    """
    domain = english word + suf
    :param suf: keyword
    :param begin: start point
    :return: list of all available domains
    """
    index = english_words.words.index(begin)

    for word in english_words.words[index:]:
        print('check %s' % word)
        time.sleep(0.5)
        if is_domain_available(word+suf):
            print('check ok %s' % word+suf)
            yield word+suf


def prefix_domain(pref, figure):
    """
    domain=pref+figure number strings
    :param pref:
    :param figure: best not bigger than 3
    :return: list of all available domains
    """
    for rand_str in figure_string(figure):
        time.sleep(0.1)
        if is_domain_available(pref+rand_str):
            yield pref+rand_str


def suffix_domain(suf, figure):
    """
    domain=figure number string+suf
    :param suf:
    :param figure: best not bigger than 3
    :return: list of all available domains
    """
    for rand_str in figure_string(figure):
        time.sleep(0.5)
        # print('check: %s' % rand_str+suf)
        if is_domain_available(rand_str+suf):
            yield rand_str+suf


def middle_domain(mid, before_figure, after_figure):
    """
    domain=before_figure number string + mid + after_figure number string
    :param mid:
    :param before_figure: best not bigger than 2
    :param after_figure: best not bigger than 2
    :return:
    """
    for rand_before_str in figure_string(before_figure):
        for rand_after_str in figure_string(after_figure):
            time.sleep(0.1)
            if is_domain_available(rand_before_str+mid+rand_after_str):
                yield rand_before_str+mid+rand_after_str


def is_domain_available(domain, extension=['com']):
    """
    check domain is available
    :param domain: domain without extension
    :param extension: extension list without dot
    :return:
    """

    for ext in extension:
        d = '%s.%s' % (domain, ext)
        try:
            print('begin query')
            r = pythonwhois.get_whois(d, True)
            print('end query')
        except Exception as e:
            with open('error.log', 'a') as file_tmp:
                file_tmp.write('check %s error:%s\n' % (d, e))
            continue

        if r.get('expiration_date', None) is None:
            return True
        if r['expiration_date'][0] < datetime.datetime.now():
            return True
        return False


def figure_string(figure, type='integer'):
    """
    遍历figure位字符串
    :param figure:
    :return:
    """
    if figure > 3:
        print('WARNING figure is too big')
    if figure <= 0:
        yield ''

    if type is 'integer':
        for i in range(10**figure):
            str10 = str(i)
            if len(str10) < figure:
                str10 = '0' * (figure - len(str10)) + str10

            yield str10
    else:
        max_num36 = 'z' * figure
        max_num10 = base36decode(max_num36)
        for i in range(max_num10 + 1):
            str36 = base36encode(i)
            # 补齐前面的0
            if len(str36) < figure:
                str36 = '0' * (figure - len(str36)) + str36
            yield str36


def base36encode(number, alphabet='0123456789abcdefghijklmnopqrstuvwxyz'):
    """
    Converts an integer to a base36 string.
    :param number:
    :param alphabet:
    :return:
    """
    if not isinstance(number, int):
        raise TypeError('number must be an integer')

    base36 = ''
    sign = ''

    if number < 0:
        sign = '-'
        number = -number

    if 0 <= number < len(alphabet):
        return sign + alphabet[number]

    while number != 0:
        number, i = divmod(number, len(alphabet))
        base36 = alphabet[i] + base36

    return sign + base36


def base36decode(number):
    return int(number, 36)


if __name__ == '__main__':
    keyword = 'block'
    figure = 3
    print('begin search---------------')
    with open('%s_%d.log' % (keyword, figure), 'a') as f:

        for domain in prefix_domain(keyword, figure):
            print('Available: %s' % domain)
            f.write(domain + '\n')

    print('end search-----------------')

    # print('begin search---------------')
    # with open('%s_word.log' % keyword, 'a') as f:
    #
    #     for domain in word_domain_suffix(keyword):
    #         print('Available: %s' % domain)
    #         f.write(domain + '\n')
    #
    # print('end search-----------------')
