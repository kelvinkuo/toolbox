#!/bin/bash

# please install chrome first, see this document http://www.apkfuns.com/centos-install-headless-chrome/
# then you need config mail, add below to your /etc/mail.rc
#   set from=guochenshu@126.com
#   set smtp=smtp.126.com
#   set smtp-auth-user=your email username
#   set smtp-auth-password=your password
#   set smtp-auth=login
#   如果发邮件失败，很有可能是邮箱未开启授权，需要到邮箱网站上去开启

price=`google-chrome-stable --headless --disable-gpu --dump-dom https://item.jd.com/2302819.html | grep 'price J-p-2302819' | sed -n 's:.*<span class="price J-p-2302819">\(.*\)\.00</span></span>.*:\1:p' | tr -d "[:space:]"`
echo `date +%Y/%m/%d-%H:%M:%S` $price >> /home/guochenshu/work/jd-price/price.dat

if [ -z "$price" ]; then
        exit
fi

if [[ $price -le 3399 ]]; then
	echo "快用你的小贱手点开 https://item.jd.com/2302819.html" | mail -s "震惊！博世洗衣机 $price 了" "guochenshu@xiaozhu.com,Zhouting@cyou-inc.com"
else
	echo no
fi
