#include <stdio.h>

int main(int argc, char *argv[])
{
    int a = 0xFFFFFFFF;
	a = a >> 8;
	unsigned char *p = (unsigned char*)&a;

	if(*(p+3) == 0xFF) {
		printf("arithmetic right shift\n");
	} else {
		printf("logical right shift\n");
	}
	
    return 0;
}
