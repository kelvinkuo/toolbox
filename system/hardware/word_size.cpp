#include <stdio.h>

#define SIZE_INT sizeof(int)
#define SIZE_SHORT_INT sizeof(short int)
#define SIZE_LONG_INT sizeof(long int)
#define SIZE_POINTER sizeof(void*)

int main()
{
	printf("int size %ld\n", SIZE_INT);
	printf("short int size %ld\n", SIZE_SHORT_INT);
	printf("long int size %ld\n", SIZE_LONG_INT);
	printf("pointer size %ld\n", SIZE_POINTER);
	
	return 0;
}
