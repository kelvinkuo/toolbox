#include <stdio.h>

int main(int argc, char *argv[])
{
    int a = 0x12345678;
	char *p = (char*)&a;
	if(*p == 0x78 && *(p+1) == 0x56 && *(p+2) == 0x34 && *(p+3) == 0x12) {
		printf("little endian\n");
	} else if(*p == 0x12 && *(p+1) == 0x34 && *(p+2) == 0x56 && *(p+3) == 0x78) {
		printf("big endian\n");
	} else {
		printf("unknown system\n");
	}
    return 0;
};
